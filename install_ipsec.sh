#centos6安装ipsec vpn
#for ios/mac/android

#1.Install strongSwan
yum -y install gdm gdm-devle
yum install strongSwan openssl

#2.Generate certificates
#也可以
#wget https://raw.githubusercontent.com/michael-loo/strongswan_config/for_vultr/server_key.sh
#chmod a+x server_key.sh
#wget https://raw.githubusercontent.com/michael-loo/strongswan_config/for_vultr/client_key.sh
#chmod a+x client_key.sh
#

cd /etc/strongswan/ipsec.d

vim server_key.sh #内容如下
    #!/bin/bash
    if [ $1 ];  then
        CN=$1
        echo "generating keys for $CN ..."
    else
        echo "usage:\n sh server_key.sh YOUR EXACT HOST NAME or SERVER IP\n Run this script in directory to store your keys"
        exit 1
    fi

    mkdir -p private && mkdir -p cacerts && mkdir -p certs

    strongswan pki --gen --type rsa --size 4096 --outform pem > private/strongswanKey.pem
    strongswan pki --self --ca --lifetime 3650 --in private/strongswanKey.pem --type rsa --dn "C=CH, O=VULTR-VPS-CENTOS, CN=$CN" --outform pem > cacerts/strongswanCert.pem
    echo 'CA certs at cacerts/strongswanCert.pem\n'
    strongswan pki --print --in cacerts/strongswanCert.pem

    sleep 1
    echo "\ngenerating server keys ..."
    strongswan pki --gen --type rsa --size 2048 --outform pem > private/vpnHostKey.pem
    strongswan pki --pub --in private/vpnHostKey.pem --type rsa | \
        strongswan pki --issue --lifetime 730 \
        --cacert cacerts/strongswanCert.pem \
        --cakey private/strongswanKey.pem \
        --dn "C=CH, O=VULTR-VPS-CENTOS, CN=$CN" \
        --san $CN \
        --flag serverAuth --flag ikeIntermediate \
        --outform pem > certs/vpnHostCert.pem
    echo "vpn server cert at certs/vpnHostCert.pem\n"
    strongswan pki --print --in certs/vpnHostCert.pem


vim client_key.sh #内容如下
    #!/bin/bash
    info="usage:\n sh client_key.sh USER_NAME EMAIL \n Run this script in directory to store your keys"

    if [ $1 ];  then
        if [ $2 ]; then
            NAME=$1
            MAIL=$2
            echo "generating keys for $NAME $MAIL ..."
        else
            echo $info
            exit 1
        fi
    else
        echo $info
        exit 1
    fi

    mkdir -p private && mkdir -p cacerts && mkdir -p certs

    keyfile="private/"$NAME"Key.pem"

    certfile="certs/"$NAME"Cert.pem"

    p12file=$NAME".p12"

    strongswan pki --gen --type rsa --size 2048 \
        --outform pem \
        > $keyfile

    strongswan pki --pub --in $keyfile --type rsa | \
        strongswan pki --issue --lifetime 730 \
        --cacert cacerts/strongswanCert.pem \
        --cakey private/strongswanKey.pem \
        --dn "C=CH, O=VULTR-VPS-CENTOS, CN=$MAIL" \
        --san $MAIL \
        --outform pem > $certfile

    strongswan pki --print --in $certfile

    echo "\nEnter password to protect p12 cert for $NAME"
    openssl pkcs12 -export -inkey $keyfile \
        -in $certfile -name "$NAME's VPN Certificate" \
        -certfile cacerts/strongswanCert.pem \
        -caname "strongSwan Root CA" \
        -out $p12file

    if [ $? -eq 0 ]; then
        echo "cert for $NAME at $p12file"
    fi

./server_key.sh SERVER_IP
./client_key.sh john john@gmail.com

下载 /etc/strongswan/ipsec.d/john.p12 和 /etc/strongswan/ipsec.d/cacerts/strongswanCert.pem 安装到的PC或IOS上

#3.Configure strongSwan

echo > /etc/strongswan/ipsec.conf
vim /etc/strongswan/ipsec.conf #添加如下内容
    config setup
        uniqueids=never
        charondebug="cfg 2, dmn 2, ike 2, net 0"

    conn %default
        left=%defaultroute
        leftsubnet=0.0.0.0/0
        leftcert=vpnHostCert.pem
        right=%any
        rightsourceip=172.16.1.100/16

    conn CiscoIPSec
        keyexchange=ikev1
        fragmentation=yes
        rightauth=pubkey
        rightauth2=xauth
        leftsendcert=always
        rekey=no
        auto=add

    conn XauthPsk
        keyexchange=ikev1
        leftauth=psk
        rightauth=psk
        rightauth2=xauth
        auto=add

    conn IpsecIKEv2
        keyexchange=ikev2
        leftauth=pubkey
        rightauth=pubkey
        leftsendcert=always
        auto=add

    conn IpsecIKEv2-EAP
        keyexchange=ikev2
        ike=aes256-sha1-modp1024!
        rekey=no
        leftauth=pubkey
        leftsendcert=always
        rightauth=eap-mschapv2
        eap_identity=%any
        auto=add


echo > /etc/strongswan/strongswan.conf
vim /etc/strongswan/strongswan.conf #添加如下内容
    charon {
        load_modular = yes
        duplicheck.enable = no
        compress = yes
        plugins {
                include strongswan.d/charon/*.conf
        }
        dns1 = 8.8.8.8
        dns2 = 8.8.4.4
        nbns1 = 8.8.8.8
        nbns2 = 8.8.4.4
    }

    include strongswan.d/*.conf


vim /etc/strongswan/ipsec.secrets #这个文件不存在,自己创建;添加如下内容
    : RSA vpnHostKey.pem
    : PSK "PSK_KEY"
    john %any : EAP "John's Password"
    john %any : XAUTH "John's Password"  #for ios



#Allow IPv4 forwarding
vim /etc/sysctl.conf #修改
    net.ipv4.ip_forward=1
sysctl -p


#Configure the firewall
##查看是否为eth0
ip route show | grep '^default' | sed -e 's/.* dev \([^ ]*\).*/\1/'

##添加防火墙规则
iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -s 10.31.2.0/24  -j ACCEPT
iptables -A INPUT -i eth0 -p esp -j ACCEPT
iptables -A INPUT -i eth0 -p udp --dport 500 -j ACCEPT
iptables -A INPUT -i eth0 -p tcp --dport 500 -j ACCEPT
iptables -A INPUT -i eth0 -p udp --dport 4500 -j ACCEPT
iptables -A FORWARD -j REJECT
iptables -t nat -A POSTROUTING -s 10.31.2.0/24 -o eth0 -j MASQUERADE

iptables -t mangle -A FORWARD -o eth0 -p tcp -m tcp --tcp-flags SYN,RST SYN -m tcpmss --mss 1361:1536 -j TCPMSS --set-mss 1360

#Start VPN
/etc/init.d/srongswan start|stop|restart

#Test Your VPN
##debug log
sudo cat > /usr/local/etc/strongswan.d/charon-logging.conf << EOF
charon {
    filelog {
        /var/log/strongswan.log {
            append = yes
            default = 1
            flush_line = yes
            ike_name = yes
            time_format = %b %e %T
        }
    }
}
EOF

##on ios
将.p12文件发邮件给自己,然后安装
服务器，都是 IP 或都是 URL
账户和密码写 ipsec.secrets 里 XAUTH 前后的那两个
如果要使用证书，证书选刚才的那个。否则可以不使用证书，输入 ipsec.secrets 里设置的 PSK 密码。

##on pc

##on android