CentOS6搭建Git服务器

1.服务器端安装git软件
yum install curl-devel expat-devel gettext-devel openssl-devel zlib-devel perl-devel git

2.服务器端安装gitosis
yum -y install python python-setuptools
git clone git://github.com/res0nat0r/gitosis.git
cd gitosis/
python setup.py install

3.服务器端添加git用户
useradd git
passwd git

4.windows客户端创建客户端登 陆证书
#step1: #生成id_rsa.pub文件
ssh-keygen -t rsa

#step2:
将生成的id_rsa.pub文件上传到git服务器上的/home/git/

5.服务器端git上操作：
使用root用户生成管理库:
sudo -H -u git gitosis-init < id_rsa.pub

  	注解：
  	1. 生成的gitosis-admin为Git的用户访问权限管理库，gitosis通过这个git库来管理所有git库的访问权限。

    2. 通过执行初始化，该公钥的拥有者就能修改用于配置gitosis的那个特殊Git仓库了
    	修改上传权限：
        chmod 755 /home/git/repositories/gitosis-admin.git/hooks/post-update

6.在windows客户端操作:
	git clone git@192.168.189.128:/home/git/repositories/gitosis-admin.git

  	添加新的用户到项目中:
  	cd gitosis-admin/
  	将用户的公钥文件【如：testuser@HD3.pub】复制到keydir/

          修改gitosis.conf添加如下
               [group git-do123]            # 具有写权限的组名称
               writable = git-do123       # 该组可写的项目名称 test-git是项目名称
               members = ltl@jackliu-ThinkPad  guangyun.ni@yeepay.com     #该组的成员(密钥用户名) 多个用户协同开发时，以空格分隔

               # 如果要增加只读的组 参考如下
               # [group test-git-readnoly]          # 具有都权限的组名称
               # readonly = git-do123              # 该组只读的项目名称
               # members = ltl@jackliu-ThinkPad     # 该组的成员
    git add .
    git commit -a -m "add user xxxx"
    git push

7.在windows客户端初始化项目项目
	mkdir gitproj
	cd gitproj
	git --bare init

	mkdir git-do123
	cd git-do123
	git init

	添加文件README.md文件
	git add .
	git commit -a -m "init test-git"
	git remote add origin git@192.168.12.20:test-git.git
	git push origin master

8.服务器端上禁止git通过shell登陆
    vim /etc/passwd
    git:x:1001:1001:git version control:/home/git:/usr/bin/git-shell

	git客户端配置:
	git clone git@domain.com:/data/git/project.git