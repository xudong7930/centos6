#!/usr/bin/env bash

#安装路径
pathdir=/usr/local/mongodb

#下载文件
wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.2.8.tgz

tar -zxf mongodb-linux-x86_64-3.2.8.tgz

mv mongodb $pathdir

mkdir $pathdir/data

touch $pathdir/mongodb.log

#创建软连接
ln -s $pathdir/bin/* /usr/local/bin/

#创建配置文件,将mongo.conf放到目录下,并根据需要适当调整配置
mv mongo.conf $pathdir/

#创建windows启动脚本
#start.bat
@echo off
set BASEDIR=D:/wnmp
set MONGODIR=%BASEDIR%/mongodb

echo Starting Mongodb...
RunHiddenConsole %MONGODIR%/bin/mongod.exe --config %MONGODIR%/mongo.conf

#stop.bat
@echo off
echo Stopping Mongodb...
taskkill /F /IM mongod.exe > nul
exit

#创建linux启动脚本
#!/bin/bash
# mongod - this script starts and stops the mongo database daemon
#
# description:
# config:
#

# source function library
. /etc/rc.d/init.d/functions

# source networking configure
. /etc/sysconfig/network

# check that networking is up.
[ "$NETWORKING" = "no" ] && exit 0

mongofile=/usr/local/mongodb/bin/mongod
conffile=/usr/local/mongodb/mongo.conf

#start function
mongod_start(){
    $mongofile --config $conffile
   	}

#stop function
mongod_stop(){
    killall -2 mongod
    }

case "$1" in
start)
    echo "start..."
    mongod_start
    if [ $? == 0 ];then
        echo "Secuss start MongoDB!"
        netstat -antp | grep mongo
    fi
    ;;
stop)
    mongod_stop
    if [ $? == 0 ];then
        echo "MongoDB is shutdown now !"
    fi
    ;;
restart)
    mongod_stop
    mongod_start
    ;;
status)
    netstat -antp | grep mongo
    ;;
*)
    echo "Use args (start|stop|restart|status)"
    ;;
esac


