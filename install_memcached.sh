CentOS安装memcached

yum -y install libevent libevent-devel

wget http://memcached.org/memcached-1.4.25.tar.gz
tar -zxvf memcached-1.4.25.tar.gz
cd memcached-1.x.x
./configure --prefix=/usr/local/memcached
make && make test
sudo make install

#查看帮助
/usr/local/memcached/bin/memcached -h

#启动命令
/usr/local/memcached/bin/memcached -p 11211 -m 64m -d
/usr/local/memcached/bin/memcached -d -m 64M -u root -l 192.168.0.200 -p 11211 -c 256 -P /tmp/memcached.pid

#启动选项
-d是启动一个守护进程；
-m是分配给Memcache使用的内存数量，单位是MB；
-u是运行Memcache的用户；
-l是监听的服务器IP地址，可以有多个地址；
-p是设置Memcache监听的端口，，最好是1024以上的端口；
-c是最大运行的并发连接数，默认是1024；
-P是设置保存Memcache的pid文件。
-vv 显示调试信息

#命令行链接到Memcached
语法: telnet 主机 端口
telnet 127.0.0.1 11211

启动脚本:
#!/bin/sh
#
# memcached    Startup script for memcached processes
#
# chkconfig: - 90 10
# description: Memcache provides fast memory based storage.
# processname: memcached

[ -f memcached ] || exit 0

memcached="/usr/local/memcached/bin/memcached"
prog=$(basename  $memcached)
port=11211
user=nobody

# memory use
mem=64

start() {
    echo -n $"Starting $prog "
    # Starting memcached with 64MB memory on port 11211 as deamon and user nobody
    $memcached -m $mem -p $port -d -u $user

    RETVAL=$?
    echo
    return $RETVAL
}

stop() {
    if test "x`pidof memcached`" != x; then
        echo -n $"Stopping $prog "
        killall memcached
        echo
        fi
    RETVAL=$?
    return $RETVAL
}

case "$1" in
        start)
            start
            ;;

        stop)
            stop
            ;;

        restart)
            stop
            start
            ;;
        condrestart)
            if test "x`pidof memcached`" != x; then
                stop
                start
            fi
            ;;

        *)
            echo $"Usage: $0 {start|stop|restart|condrestart}"
            exit 1

esac

exit $RETVAL