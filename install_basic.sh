#!/bin/bash
#
# install_basic.sh - this script install centos some basic tools and soft
#
# @author: xudong7930 <xudong7930@gmail.com>
# @date: 2016年04月17日12:36:15
# @description: install some baisc tools an soft
#

# basic tools
yum -y install gcc gcc-c++ cmake make autoconf automake glibc

# some software
yum -y install vim unrar nmap wget



