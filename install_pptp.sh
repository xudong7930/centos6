#
#  CentOS 6 install-pptpd.sh
#
#  Created by zhaodg on 15-04-26.
#  Copyright (c) 2014年 zhaodg. All rights reserved.
#

#!/bin/bash

# clean
yum -y remove pptpd ppp

iptables --flush POSTROUTING --table nat
iptables --flush FORWARD

rm -rf /etc/pptpd.conf
rm -rf /etc/ppp
rm -rf /dev/ppp

# install component
yum -y install make openssl gcc-c++ ppp iptables pptpd iptables-services


#define pre-const
user="xudong7930"
passwd="abcd1234"
localip="192.168.100.1"
remoteip_range="192.168.100.100-238"
netmask="192.168.100.0/24";


# /etc/ppp/chap-secrets
echo "${user} pptpd ${passwd} *" >> /etc/ppp/chap-secrets


# /etc/pptpd.conf
echo "localip ${localip}" >> /etc/pptpd.conf
echo "remoteip ${remoteip_range}" >> /etc/pptpd.conf

# /etc/ppp/options.pptpd
echo "ms-dns 8.8.8.8" >> /etc/ppp/options.pptpd
echo "ms-dns 8.8.4.4" >> /etc/ppp/options.pptpd

# /etc/sysctl.conf
echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
sysctl -p # 使内核转发生效

#设置MTU来确保过大的包不会被丢弃
iptables -A FORWARD -p tcp --syn -s ${netmask} -j TCPMSS --set-mss 1356

#iptables -t nat -A POSTROUTING -s 172.16.36.0/24 -j SNAT --to-source `ifconfig | grep 'inet' | grep 'netmask' | grep 'broadcast' | grep -v '127.0.0.1' | cut -d: -f2 | awk 'NR==1 {print $2}'`

#注意这里eth0代表你的外网网卡，请用ifconfig查看或者咨询网络管理员
iptables -t nat -A POSTROUTING -s ${netmask} -o eth0 -j MASQUERADE

#如果上面的命令报错,那么可以尝试以下的命令，其中xxx.xxx.xxx.xxx代表你的VPS外网ip地址
#sudo iptables -t nat -A POSTROUTING -s 192.168.0.0/24 -o eth0 -j SNAT --to-source xxx.xxx.xxx.xxx


/usr/libexec/iptables/iptables.init save

mknod /dev/ppp c 108 0
chmod +x /etc/rc.d/rc.local
echo "1" > /proc/sys/net/ipv4/ip_forward
echo "mknod /dev/ppp c 108 0" >> /etc/rc.local
echo "echo \"1\">/proc/sys/net/ipv4/ip_forward" >> /etc/rc.local

echo "iptables -A INPUT -p tcp  --dport 1723 -j ACCEPT" >> /etc/rc.local
echo "iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT" >> /etc/rc.local
echo "iptables -A INPUT -p gre -j ACCEPT" >> /etc/rc.local
echo "iptables -A OUTPUT  -p gre -j ACCEPT" >> /etc/rc.local

echo "iptables -A FORWARD -p tcp --syn -s ${netmask} -j TCPMSS --set-mss 1356" >> /etc/rc.local
echo "iptables -t nat -A POSTROUTING -s ${netmask} -o eth0 -j MASQUERADE" >> /etc/rc.local

service iptables restart
service pptpd restart

iptables -t nat -A POSTROUTING -s ${netmask} -o eth0 -j MASQUERADE

# 开机自动启动
chkconfig pptpd on

echo "PPTP VPN is installed, your VPN username is ${user}, VPN password is ${passwd}"
