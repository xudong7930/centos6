<?php


$a = 28470 * 2.5 / 10000; //7.1175
$b = 28470 * 2/100000; //0.5694
$c = 28470 * 1/1000; //28.47

echo 31.32 - $b - $c; //2.28

// (new Stock(2.2, true))->calcBuy(24.08, 1);
// (new Stock(2.5, false))->calcMargin(200.443, 2, 208, 1);

class Stock
{
    // 券商佣金费率
    public $brokerFeeRate = 2.5 / 10000;

    // 券商佣金是否免5
    public $brokerHasMinFee = true;
    
    // 过户费费率
    public $transferFeeRate = 2/100000;

    // 印花税费率
    public $stampFeeRate = 1/1000;

    // 交易类型
    const TRANSFER_TYPE_BUY = 1;
    const TRANSFER_TYPE_SELL = 2;

    public function __construct($brokerFeeRate, $brokerHasMinFee=true, $transferFeeRate=2/100000)
    {
        $this->brokerFeeRate = $brokerFeeRate/10000;
        $this->brokerHasMinFee = $brokerHasMinFee;
        $this->transferFeeRate = $transferFeeRate;
    }

    public function calcMargin($holdPrice, $holdAmount, $actPrice, $actAmount)
    {
        $holdTotal = $holdPrice * $holdAmount * 100;
        
        $actTotal = $actPrice * $actAmount * 100;
        $brokerCost = $this->calcBrokerFee($actTotal);
        $transferCost = $this->calcTransferFee($actTotal);
        $stampCost = $this->calcStampFee($actTotal, self::TRANSFER_TYPE_SELL);

        $actFee = $brokerCost*2 + $transferCost*2 + $stampCost;

        $avgPrice = ($holdTotal + $actTotal + $actFee) / ($holdAmount + $actAmount) / 100;
        $avgPrice = round($avgPrice, 3);

        $actDesc = '';
        if ($actAmount > 0) {
            $actDesc = "买入";
        } else {
            $actDesc = "卖出";
        }
        echo sprintf("%s价%s,数量%s手后, 成本%s(佣金:%s,过户费:%s,印花税:%s), 均价:%s,数量:%s",
            $actDesc,$actPrice, $actAmount,$actFee,$brokerCost*2,$transferCost*2,$stampCost, $avgPrice, $holdAmount+$actAmount
        );
    }

    /**
     * 买入成本计算
     */
    public function calcBuy($buyPrice, $buyAmount)
    {
        $total = $buyPrice * $buyAmount * 100;
        $brokerCost = $this->calcBrokerFee($total);
        $transferCost = $this->calcTransferFee($total);
        $stampCost = $this->calcStampFee($total, self::TRANSFER_TYPE_BUY);
        $stampCost2 = $this->calcStampFee($total, self::TRANSFER_TYPE_SELL);

        // 券商成本
        $totalCost = $brokerCost*2 + $transferCost*2 + $stampCost + $stampCost2;
        // $totalCost = $brokerCost + $transferCost + $stampCost;

        // 平均成本
        $avgPrice = ($total + $totalCost ) / ($buyAmount * 100);
        $avgPrice = round($avgPrice, 3);

        echo sprintf("买入价:%s, 数量:%s, 成本:%s(佣金/过户/印花税:[买入:%s, %s, %s; 卖出:%s, %s, %s]), 均价:%s", 
            $buyPrice, $buyAmount, $totalCost, 
            $brokerCost, $transferCost, $stampCost, 
            $brokerCost, $transferCost, $stampCost2, 
            $avgPrice
        );
    }

    /**
     * 计算券商佣金
     */
    private function calcBrokerFee($total)
    {
        $fee = 0;
        if($this->brokerHasMinFee) {
            $minFee = 5;
            // 达到最小费用的交易额
            $middleCost = $minFee / $this->brokerFeeRate;
            $fee = ($total > $middleCost) ? $total * $this->brokerFeeRate : $minFee;
        } else {
            $fee = $total * $this->brokerFeeRate;
        }

        return round($fee, 2);
    }

    /**
     * 计算过户费
     */
    private function calcTransferFee($total)
    {
        $fee = $total * $this->transferFeeRate;
        return round($fee, 2);
    }

    /**
     * 计算印花税费
     */
    private function calcStampFee($total, $type)
    {
        $fee = 0;
        if ($type == self::TRANSFER_TYPE_SELL) {
            $fee = $total * $this->stampFeeRate;
        }
        return round($fee, 2);
    }
}
