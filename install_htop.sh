#!/bin/bash
#
# install_htop.sh - this script install htop automatic
#
# chkconfig:   - 85 15
# description: how to install htop soft
#

# isntall dependency
yum -y install ncurses-devel

# download and install
wget http://pkgs.fedoraproject.org/repo/pkgs/htop/htop-1.0.tar.gz/325112ca7947ea1f6d6441f631e00384/htop-1.0.tar.gz

tar -zxf htop-1.0.tar.gz

cd htop-1.0

./configure && make && make install

cd ../

rm -fr htop*
